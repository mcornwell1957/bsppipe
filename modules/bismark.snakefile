#MODULE: Run Bismark On the fastq files
#PARAMETERS:
_logfile="logs/bismark.log"

def getFastq(wildcards):
    return config["samples"][wildcards.sample]

def bismark_targets(wildcards):
    """Generates the targets for this module"""
    ls = []
    for sample in config["samples"]:
        ls.append("data/" + config["species"] + "_Bisulfite_Genome.done")
        ls.append("data/" + config["token"] + "/%s/%s.bam" %(sample,sample))
        ls.append("analysis/" + config["token"] + "/%s/%s_bismark/CpG_context_%s.txt.gz" %(sample,sample,sample))
        ls.append("analysis/" + config["token"] + "/%s/%s_bismark/%s.bismark.cov" %(sample,sample,sample))
    return ls

rule bismark_genome_prep:
    input:
        config["fastaRefDir"]
    output:
        donefile = "data/" + config["species"] + "_Bisulfite_Genome.done"
    resources: mem=config["RAM_usage"]
    message: "Prep the reference fasta file for Bismark analysis"
    run:
        shell( " bismark_genome_preparation --bowtie2 --genomic_composition --verbose {input} " )
        shell( " touch {output.donefile} " )

rule bismark:
    input:
        getFastq,
        genome_check = "data/" + config["species"] + "_Bisulfite_Genome.done"
    output:
        "data/" + config["token"] + "/{sample}/{sample}.bam"
    params:
        bismarkref = config["fastaRefDir"],
        bismarkout = "data/" + config["token"] + "/{sample}/",
        basename = "{sample}"
    resources: mem=config["RAM_usage"]
    message: "Run Bismark on {wildcards.sample}"
    log: _logfile
    run:
        if len(input) > 2:
            _inputs=("-1 %s -2 %s" % (input[0], input[1]))
            _output=("data/" + config["token"] + "/" + params.basename + "/" + params.basename + "_pe.bam" )
            shell( "bismark {params.bismarkref} {_inputs} --bowtie2 -o {params.bismarkout} -B {params.basename} 2>>{log}" )
            shell( "mv {_output} {output}" )
        else:
            #SE
            _inputs=" --se %s" % input[0]
            _output=("data/" + config["token"] + "/" + params.basename + "/" + params.basename + ".bam")
            shell( "bismark {params.bismarkref} {_inputs} --bowtie2 -o {params.bismarkout} -B {params.basename} 2>>{log}" )

rule bismark_meth_extract:
    input:
        bam = "data/" + config["token"] + "/{sample}/{sample}.bam",
        fastqfile = getFastq
    output:
        CPGfile = "analysis/" + config["token"] + "/{sample}/{sample}_bismark/CpG_context_{sample}.txt.gz",
        bedgraph = "analysis/" + config["token"] + "/{sample}/{sample}_bismark/{sample}.bedGraph.gz",
        covfile = "analysis/" + config["token"] + "/{sample}/{sample}_bismark/{sample}.bismark.cov.gz"
    params:
        outpath = "analysis/" + config["token"] + "/{sample}/{sample}_bismark/"
    resources: mem=config["RAM_usage"]
    message: "Run bismark methylation extraction on {wildcards.sample}"
    run:
        print({input.fastqfile})
        if len(input.fastqfile) > 1:
            #PE
            _inparams = "-p"
        else:
            #SE
            _inparams = ""
        shell( " bismark_methylation_extractor --comprehensive {_inparams} --bedGraph {input.bam} -o {params.outpath} --gzip " )


rule prepare_coverage_files:
    input:
        bedgraph = "analysis/" + config["token"] + "/{sample}/{sample}_bismark/{sample}.bedGraph.gz",
        covfile = "analysis/" + config["token"] + "/{sample}/{sample}_bismark/{sample}.bismark.cov.gz"
    output:
        bedgraphout = "analysis/" + config["token"] + "/{sample}/{sample}_bismark/{sample}.bedGraph",
        covfileout = "analysis/" + config["token"] + "/{sample}/{sample}_bismark/{sample}.bismark.cov"
    shell:
        " gunzip {input.bedgraph} "
        " && gunzip {input.covfile} "    