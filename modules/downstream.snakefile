#PARAMETERS:

def downstream_targets(wildcards):
    """Generates the targets for this module"""
    ls = []
    for sample in config["samples"]:
        ls.append("analysis/" + config["token"] + "/%s/%s_complete_table.txt" %(sample,sample))
        ls.append("analysis/" + config["token"] + "/%s/%s_QC/%s_peaks.fa" %(sample,sample,sample))
        ls.append("analysis/" + config["token"] + "/%s/%s_QC/%s_peaks_fasta_counts.txt" %(sample,sample,sample))
        ls.append("analysis/" + config["token"] + "/%s/%s_QC/%s_reference_fasta_counts.txt" %(sample,sample,sample))
        ls.append("analysis/" + config["token"] + "/%s/%s_CpG_bias_stats.txt" %(sample,sample))
        ls.append("analysis/" + config["token"] + "/%s/%s_plots/Cpg_Methylation_vs_readcount.pdf" %(sample,sample))
    return ls

rule make_complete_table:
    input:
        countfile = "analysis/" + config["token"] + "/{sample}/{sample}_counts.txt",
        covfile = "analysis/" + config["token"] + "/{sample}/{sample}_bismark/{sample}.bismark.cov"
    output:
        completetab = "analysis/" + config["token"] + "/{sample}/{sample}_complete_table.txt"
    shell:
        " Rscript modules/scripts/Create_Methylation_Binding_Table.R -ic {input.countfile} -im {input.covfile} -o {output}"


## CpG Bias QC
rule make_fasta_from_peakome:
    input:
        fastaRef = config["fastaRef"],
        peakome = "analysis/" + config["token"] + "/{sample}/{sample}_peaks.bed"
    output:
        peakFasta = "analysis/" + config["token"] + "/{sample}/{sample}_QC/{sample}_peaks.fa"
    shell:
        " bedtools getfasta -fi {input.fastaRef} -bed {input.peakome} -fo {output.peakFasta} "

rule extract_count_from_peak_fasta:
    input:
        "analysis/" + config["token"] + "/{sample}/{sample}_QC/{sample}_peaks.fa"
    output:
        "analysis/" + config["token"] + "/{sample}/{sample}_QC/{sample}_peaks_fasta_counts.txt"
    shell:
        " faCount {input} > {output} "

rule extract_count_from_reference_fasta:
    input:
        fastaRef = config["fastaRef"]
    output:
        "analysis/" + config["token"] + "/{sample}/{sample}_QC/{sample}_reference_fasta_counts.txt"
    shell:
        " faCount {input} > {output} "

rule calculate_CpG_bias_statistics:
    input:
        RefFastaCount = "analysis/" + config["token"] + "/{sample}/{sample}_QC/{sample}_reference_fasta_counts.txt",
        PeakFastaCount = "analysis/" + config["token"] + "/{sample}/{sample}_QC/{sample}_peaks_fasta_counts.txt",
        completetab = "analysis/" + config["token"] + "/{sample}/{sample}_complete_table.txt"
    output:
        sumtab = "analysis/" + config["token"] + "/{sample}/{sample}_CpG_bias_stats.txt"
    shell:
        " Rscript modules/scripts/Calculate_CpG_Bias_Stats.R -ir {input.RefFastaCount} -ip {input.PeakFastaCount} -c {input.completetab} -o {output.sumtab} "

rule create_R_plots:
    input:
        completetab = "analysis/" + config["token"] + "/{sample}/{sample}_complete_table.txt"
    output:
        "analysis/" + config["token"] + "/{sample}/{sample}_plots/Cpg_Methylation_vs_readcount.pdf",
        "analysis/" + config["token"] + "/{sample}/{sample}_plots/Peak_Methylation_vs_readcount.pdf",
        "analysis/" + config["token"] + "/{sample}/{sample}_plots/Percent_Methylation_vs_Distance_From_Peak_Summit.pdf",
        "analysis/" + config["token"] + "/{sample}/{sample}_plots/Percent_Methylation_vs_Binned_Distance_From_Peak_Summit.pdf"
    params:
        "analysis/" + config["token"] + "/{sample}/{sample}_plots/"
    shell:
        " mkdir -p {params} "
        " && Rscript modules/scripts/Methylation_Binding_Plots.R -i {input} -o {params} "

