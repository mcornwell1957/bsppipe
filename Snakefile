## Import python packages
import pandas as pd
from modules.scripts.metasheet_setup import updateMeta
from modules.scripts.config_setup import updateConfig

## Designate config file
configfile: "config.yaml"
#config = updateMeta(config)
config = updateConfig(config)

#def addPy2Paths_Config(config):
#    """ADDS the python2 paths to config"""
#    conda_root = subprocess.check_output('conda info --root',shell=True).decode('utf-8').strip()
#    conda_path = os.path.join(conda_root, 'pkgs')
#    config["python2_pythonpath"] = os.path.join(conda_root, 'envs', 'chips_py2', 'lib', 'python2.7', 'site-packages')
#    
#    if not "python2" in config or not config["python2"]:
#        config["python2"] = os.path.join(conda_root, 'envs', 'chips_py2', 'bin', 'python2.7')
#
#    if not "mdseqpos_path" in config or not config["mdseqpos_path"]:
#        config["mdseqpos_path"] = os.path.join(conda_root, 'envs', 'chips_py2', 'bin', 'MDSeqPos.py')
#
#    if not "macs2_path" in config or not config["macs2_path"]:
#        config["macs2_path"] = os.path.join(conda_root, 'envs', 'chips_py2', 'bin', 'macs2')

def all_targets(wildcards):
    ls = []
    #IMPORT all of the module targets
    ls.extend(bismark_targets(wildcards))
    ls.extend(peakome_targets(wildcards))
    ls.extend(htseq_targets(wildcards))
    ls.extend(downstream_targets(wildcards))
    return ls

rule target:
    input:
        all_targets,

    message: "Compiling all results"

include: "./modules/bismark.snakefile"
include: "./modules/peakome.snakefile"
include: "./modules/htseq.snakefile"
include: "./modules/downstream.snakefile"
