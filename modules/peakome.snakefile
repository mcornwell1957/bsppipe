#PARAMETERS:
from sys import platform
if platform == "linux" or platform == "linux2":
    peakome_env = "envs/BSPpipe_py2_linux.yml"
elif platform == "darwin":
    peakome_env = "envs/BSPpipe_py2_mac.yml"

def peakome_targets(wildcards):
    """Generates the targets for this module"""
    ls = []
    for sample in config["samples"]:
        ls.append("data/" + config["token"] + "/%s/%s_sorted.bam" %(sample,sample))
        ls.append("data/" + config["token"] + "/%s/%s_rmdup_sorted.bam" %(sample,sample))
        ls.append("data/" + config["token"] + "/%s/%s_rmdup_sorted.bam.bai" %(sample,sample))
        ls.append("analysis/" + config["token"] + "/%s/%s_peaks.bed" %(sample,sample))
    return ls

"analysis/" + config["token"] + "/{sample}/{sample}_MACS/{sample}_peaks.narrowPeak"

rule sort_bamfiles:
    input:
        "data/" + config["token"] + "/{sample}/{sample}.bam"
    output:
        "data/" + config["token"] + "/{sample}/{sample}_sorted.bam"
    message: "sorting {wildcards.sample} bamfile"
    resources: mem=config["RAM_usage"]
    shell:
        " samtools sort {input} -o {output} "

rule remove_duplicates:
    input:
        "data/" + config["token"] + "/{sample}/{sample}_sorted.bam"
    output:
        "data/" + config["token"] + "/{sample}/{sample}_rmdup_sorted.bam"
    params:
        "data/" + config["token"] + "/{sample}/{sample}_rmdup_sorted_metrics.txt"
    message: "removing duplicates for {wildcards.sample}"
    resources : mem=config["RAM_usage"]
    shell:
        " picard MarkDuplicates I={input} O={output} M={params} REMOVE_DUPLICATES=true ASSUME_SORTED=true "

rule index_bamfiles:
    input:
        "data/" + config["token"] + "/{sample}/{sample}_rmdup_sorted.bam"
    output:
        "data/" + config["token"] + "/{sample}/{sample}_rmdup_sorted.bam.bai"
    message: "indexing {wildcards.sample} bam file"
    resources: mem=config["RAM_usage"]
    shell:
        " samtools index {input} "


def getFastq(wildcards):
    return config["samples"][wildcards.sample]

def getRunMode(wildcards):
    if len(getFastq(wildcards)) > 1:
        return("BAMPE")
    else:
        return("BAM")


if not config["manual_bedfile"]:
    rule extract_peakome:
        input:
            "data/" + config["token"] + "/{sample}/{sample}_sorted.bam"
        output:
            macsout = "analysis/" + config["token"] + "/{sample}/{sample}_MACS/{sample}_peaks.narrowPeak",
            finalout = "analysis/" + config["token"] + "/{sample}/{sample}_peaks.bed"
        params:
            outdir = "analysis/" + config["token"] + "/{sample}/{sample}_MACS",
            manual_bedfile_check = config["manual_bedfile"],
            runmode = getRunMode        
        message: "No manual bedfile entered, extracting peaks by running MACS on {wildcards.sample}"
        resources: mem=config["RAM_usage"]
        conda:
            peakome_env
        shell:
            " macs2 callpeak -t {input} -f {params.runmode} -g mm --outdir {params.outdir} -n {wildcards.sample} -B -q 0.01 --call-summits --cutoff-analysis "
            " && cp {output.macsout} {output.finalout} "
else:
    rule extract_peakome:
        input:
            "data/" + config["token"] + "/{sample}/{sample}_sorted.bam"
        output:
            "analysis/" + config["token"] + "/{sample}/{sample}_peaks.bed"
        params:
            manual_bedfile_check = config["manual_bedfile"]
        message: "Manual bedfile entered, copying this bed file to be used as the bed file for {wildcards.sample}"
        resources: mem=config["RAM_usage"]
        shell:
            " cp {params.manual_bedfile_check} {output} "

