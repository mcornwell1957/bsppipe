################################
# Name: MacIntosh Cornwell
# Email: mcornwell1957@gmail.com
################################


Introduction:
This is a Snakemake pipeline designed to analyze BS data in conjunction with ATAC data, or any type of genomic data. We will be uing the Miniconda3 package management system to simplify the installation process of the many tools that BSPpipe calls on. Although BSPpipe is designed to be simple to use, you will need a basic understanding of the command line system.

The only input you will need are your fastq files, and the reference fasta file for your data.

Note that there is a param within the config that will scale the RAM usage that BSPpipe uses. In addition, there are separate environment files that allow BSPpipe to be run on Linux or MacOS, more on this later.


Installation:

Note: We will be using the tool "wget" to install several things from the internet, if you are not familiar with wget or you do not have it installed, then you can just download these things manually by copy and pasting the link into your browser, and it will download it from there.

Installing Miniconda3:
We will be using the Miniconda3 package management system (aka CONDA) to manage all of the software packages that VIPER is dependent on.
Use following commands to retrieve and then RUN the Minicoda3 installation script:

	$ wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
	$ bash Miniconda3-latest-Linux-x86_64.sh

Whilst running the installation script, follow the commands listed on screen, and press the enter key to scroll.
Make sure to answer yes when asked if you want to prepend Miniconda3 to PATH.
Close your terminal, open a new one and you should now have Conda working! Test by entering:

	$ conda update conda
		Press y to confirm the conda updates
NOTE: you will only have to install Miniconda3 once.
NOTE: remember to close your terminal session and re-login

Installing the VIPER conda environments:
We are now ready to use CONDA to install the software packages which VIPER is dependent on.

	$ wget https://bitbucket.org/mcornwell1957/bsppipe/get/master.tar.gz
	$ tar -xf master.tar.gz
	$ mv mcornwell1957-BSPpipe-XXXXX BSPpipe
NOTE: the XXXXX refers to the latest changeset of viper, so it will differ

After the BSPpipe folder has been created, you will have to install the environments that BSPpipe requires to run.

Change into the environment folder within the BSPpipe program
	$ cd BSPpipe/modules/envs
	$ conda env create -f BSPpipe_py3_<OPERATING_SYSTEM>.yml -n BSPpipe
	$ conda env create -f BSPpipe_py2_<OPERATING_SYSTEM>.yml -n BSPpipe_py2
NOTE: you will only have to install the VIPER conda environments once.
NOTE: Notice that you will have to choose the proper OS file to match the OS of the computer you are installing on.


File Setup:

The config.yaml file is where you list your samples. Please note that this is a yaml file, so please only use spaces and full file names.
The information within this file is as follows:
	analysis_token: The name of the output folder, this is especially useful for when you want to rerun analyses with different things and dont want to overwrite what is already there. So use this often.

	RAM_usage: this is to scale the proper RAM usage for the machine you are using. BSPpipe has been tested on a machine with a minimum of 8GB of RAM, and 1 core. Although less may work, it is recommended that you use at least 8

	species: this is where you manually tell the pipeline what species you are using
	
	fastaRefDir: this is the directory to where the fasta file is, but not the actual fasta file itself, note that we will be writing out to this location, so make sure the fasta file and the fasta ref is in a writable location.
	
	fastaRef: this is the path to the fasta file, note that we will be writing out to this location, so make sure the fasta file and the fasta ref is in a writable location.
		NOTE: I understand this can seem redundant, but this is important to the use of the pipeline
	
	manual_bedfile: The path to the bed file for the peaks that you interogate your binding/ATAC data around. If not input, the pipeline will extract the peaks based off of the fastq files. This is a manual override if there is a particular set of regions you wish to explore
	
	samples: This is where you list your samples, please use the format already listed in the example where you enter samples using two spaces, and then below that enter 4 spaces, dash, one space, then the full path to the bam file.

You will then need to activate the bismark environment to use it using the command
	$ source activate BSPpipe


Running the Pipeline:

Please refer to snakemake notes online (https://snakemake.readthedocs.io/en/stable/) to learn how to use snakemake. The quick instructions are as followed: After you have set up your config and metasheet. Run snakemake using the following from within the folder that contains the Snakefile:

dryrun of snakemake (to see if everything will run smoothly, and to return what is about to be performed)
	$ snakemake --use-conda -n

Run snakemake 
	$ snakemake --use-conda

Please refer to the snakemake docs to learn more about how to use the pipeline


File Output:

BSPpipe outputs a number of files, and these will be separated into a "data" folder and an "analysis" folder. Within these, there will be a folder named whatever given "analysis_token" is given. 
Under the data folder, there will be a folder named after your "analysis_token". Within that folder will be some of the larger upstream files that will not be useful for analysis. This includes:
	the bam file output from bismark
	the filtered bam files that have been sorted and had duplicates removed
	the sorted sam file
	various metrics output from these steps

There will also be a folder named after your "analysis_token" within the "Analysis" folder. This includes:
	folder <sample>_MACS - that has the output of the peakfinder MACS step built into the pipeline
	file <sample>_peaks.bed - the outputted bed file from the peak caller, or if you manually input a bed file it will be copied to this location
	file <sample>_bed.gtf - the converted bed to gtf file, used in the HTseq rule
	file <sample>_counts.txt - The htseq output that shows the number of reads per peak
	folder <sample>_bismark - the output from bismark
	file <sample>_complete_table.txt - This is the super table that finds every CpG found by bismark, and aligns it to a peak, and also includes the information regarding the peak intensity and the methylation of the CpG
	folder <sample>_QC - this is the folder containing some of the preliminary QC information output by BSPpipe
	folder <sample>_plots - this folder contains a lot of useful figures regarding the methylation to peak intensity correlation, and also where the CpGs fall within the peak and their respective methylation
	file <sample>_bias_stats.txt - file that has the QC stats about your sample
	