#PARAMETERS:

def htseq_targets(wildcards):
    """Generates the targets for this module"""
    ls = []
    for sample in config["samples"]:
        ls.append("data/" + config["token"] + "/%s/%s_rmdup_sorted.sam" %(sample,sample))
        ls.append("analysis/" + config["token"] + "/%s/%s_bed.gtf" %(sample,sample))
        ls.append("analysis/" + config["token"] + "/%s/%s_counts.txt" %(sample,sample))
    return ls

rule bam_to_sam:
    input:
        "data/" + config["token"] + "/{sample}/{sample}_rmdup_sorted.bam"
    output:
        "data/" + config["token"] + "/{sample}/{sample}_rmdup_sorted.sam"
    shell:
        " samtools view -o {output} {input} "

rule bed_to_gtf:
    input:
        "analysis/" + config["token"] + "/{sample}/{sample}_peaks.bed"
    output:
        "analysis/" + config["token"] + "/{sample}/{sample}_bed.gtf"
    shell:
        " Rscript modules/scripts/3cbed_to_gtf.R -i {input} -o {output} "

rule htseq:
    input:
        gtf = "analysis/" + config["token"] + "/{sample}/{sample}_bed.gtf",
        sam = "data/" + config["token"] + "/{sample}/{sample}_rmdup_sorted.sam"
    output:
        "analysis/" + config["token"] + "/{sample}/{sample}_counts.txt"
    shell:
        " htseq-count -r pos {input.sam} {input.gtf} > {output} "  
